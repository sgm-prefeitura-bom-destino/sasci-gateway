

## Descrição

Microsserviço responsável por centralizar as chamadas ao microsserviço legado do SASCi

## Instalação

```bash
$ npm install
```

## Executar a aplicação

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Sobre

Desenvolvido por [José Victor](https://www.linkedin.com/in/jvictoralves/).

Pós Graduação em Arquitetura de Software Distribuído - PUC Minas.

