import { Module } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { AuthModule } from './auth/auth.module';
import { LoggingInterceptor } from './interceptors/logging.interceptor';
import { SasciInternalController } from './sasci-internal/sasci-internal.controller';
import { SasciController } from './sasci/sasci.controller';
import { VaultModule } from './vault/vault.module';

@Module({
  imports: [AuthModule, VaultModule],
  controllers: [SasciController, SasciInternalController],
  providers: [
    { provide: APP_INTERCEPTOR, useClass: LoggingInterceptor },
  ],
})
export class AppModule {}
