import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { VaultModule } from 'src/vault/vault.module';
import { JwtAuthGuard } from './jwt-auth.guard';
import { JwtInternalAuthGuard } from './jwt-internal-auth.guard';
import { JwtInternalStrategy } from './jwt-internal.strategy';
import { JwtModuleRegisterService } from './jwt-module-register/jwt-module-register.service';
import { JwtStrategy } from './jwt.strategy';

@Module({
  imports: [
    PassportModule,
    VaultModule,
    JwtModule.registerAsync({ useClass: JwtModuleRegisterService, imports: [VaultModule]  })
  ],
  providers: [JwtStrategy, JwtAuthGuard, JwtInternalAuthGuard, JwtInternalStrategy],
  exports: [JwtAuthGuard]
})
export class AuthModule { }
