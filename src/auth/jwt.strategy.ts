import { Injectable, Logger } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { VaultLoader } from 'src/vault/vault-loader/vault-provider.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {

  constructor(private vault : VaultLoader) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKeyProvider: (req, raw, done) => this.secretProvider(req, raw, done),
      ignoreExpiration: false
    });
  }

  private async secretProvider (req, rawJwtToken, done) {
    const secret = await this.vault.getSecret(VaultLoader.JWT_PATH, "JWT_KEY");
    done(null, secret);
  }

  async validate(payload: any) {
    return payload;
  }
}