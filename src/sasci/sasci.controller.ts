import { BadRequestException, Controller, Get, Post, Put, Query, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('sasci')
@UseGuards(JwtAuthGuard)
export class SasciController {

  @Post('/appointment')
  saveAppointment() {
  }
  
  @Get('/appointment')
  fetchAppointments() {
    return Appointments;
  }

  @Put('/appointment/reschedule')
  reschedule() {
    
  }
  
  @Put('/appointment/cancel')
  cancel() {

  }

  @Get('/doctor')
  fetchDoctor(@Query('postoId') postoId: string, @Query('date') date: string) {
    const day = new Date(date).getDate();

    if (isNaN(day)) {
      throw new BadRequestException('Data inválida');
    }

    return Doctors[day % 2];
  }


}

const Doctors = [
  { name: "Luís Sousa Peres", crm: "1232" },
  { name: "Maria de Lourdes Fonseca", crm: "3768" }
]

const Appointments = [
  { id: 1, date: "2021-02-03", posto: 3, doctor: { name: "Maria de Lourdes Fonseca", crm: "3768" }, status: "DONE" },
  { id: 2, date: "2021-05-03", posto: 1, doctor: { name: "Luis Sousa Peres", crm: "1232" }, status: "CONFIRMED" },
  { id: 3, date: "2021-07-20", posto: 3, doctor: { name: "Luis Sousa Peres", crm: "1232" }, status: "WAITING_CONFIRMATION" },
  { id: 4, date: "2021-07-22", posto: 2, doctor: { name: "Maria de Lourdes Fonseca", crm: "3768" }, status: "CANCELED" }
]
