import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

const port = process.env.PORT || '3003';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(Number(port));
}
bootstrap();
