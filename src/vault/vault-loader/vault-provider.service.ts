import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import * as NodeVault from 'node-vault';
import { Constants } from 'src/util/constants';

@Injectable()
export class VaultLoader {

  static JWT_PATH = "secret/jwt";

  private vault: NodeVault.client;
  private cache = {};
  private readonly logger = new Logger(VaultLoader.name);

  constructor() {
    this.vault = NodeVault(Constants.VAULT_SETTINGS);
  }

  async getSecret(path: string, secret: string): Promise<string> {
    const cacheResponse = this.cache[`${path}${secret}`]
    if (cacheResponse) {
      return cacheResponse;
    } else {
      const result = await this.vault.read(path);
      const response = (result?.data ?? {})[secret];
      this.cache[`${path}${secret}`] = response;
      return response;
    }
  }

}
