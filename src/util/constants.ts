export const Constants = {
  VAULT_SETTINGS: {
    endpoint: process.env.VAULT_URL || 'http://127.0.0.1:8200',
    token: process.env.VAULT_TOKEN || 'TOKEN_DEV'
  }
}