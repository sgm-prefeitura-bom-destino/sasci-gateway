import { Controller, Get, Put, UseGuards } from '@nestjs/common';
import { JwtInternalAuthGuard } from 'src/auth/jwt-internal-auth.guard';

@Controller('internal/sasci')
@UseGuards(JwtInternalAuthGuard)
export class SasciInternalController {

  @Put('/appointment/cancel')
  cancel() {

  }
  
  @Put('/appointment/approve')
  approve() {

  }

  @Get('/appointment')
  fetchScheduledAppointments() {
    return ScheduledAppointments;
  }

}

const ScheduledAppointments = [
  {
    date: "2021-03-02",
    posto: 1,
    doctor: { name: "Luis Sousa Peres", crm: "1232" },
    paciente: { name: "João da Silva", sus_number: "000 0000 0000 0000" },
    status: "WAITING_CONFIRMATION"
  },
  {
    date: "2021-03-03",
    posto: 1,
    doctor: { name: "Luis Sousa Peres", crm: "1232" },
    paciente: { name: "José de Souza", sus_number: "123 6545 0987 6543" },
    status: "CONFIRMED"
  },
  {
    date: "2021-03-04",
    posto: 1,
    doctor: { name: "Luis Sousa Peres", crm: "1232" },
    paciente: { name: "Maria Soares", sus_number: "123 4567 8901 2345" },
    status: "DONE"
  },
  {
    date: "2021-03-05",
    posto: 1,
    doctor: { name: "Viviane Medeiros", crm: "8765" },
    paciente: { name: "Juliana Morais Fernandes", sus_number: "098 8765 5435 4322" },
    status: "CANCELED"
  },
];
